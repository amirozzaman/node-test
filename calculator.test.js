const calculator = require("./calculator");

test("adds 1 + 2 to equal 3", () => {
  expect(calculator.sum(1, 2)).toBe(3);
});
test("substract 5 - 2 to equal 3", () => {
  expect(calculator.substarct(5, 2)).toBe(3);
});
test("multiply 5 * 2 to equal 10", () => {
  expect(calculator.multiply(5, 2)).toBe(10);
});
test("divide 100 / 2 to equal 50", () => {
  expect(calculator.divide(100, 2)).toBe(50);
});
